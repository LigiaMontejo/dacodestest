//
//  DetailMovieVC.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/27/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class DetailMovieVC: UIViewController {

    var data: Movie?
    var movie: MovieDetail?
    
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitleLbl: UILabel!
    @IBOutlet weak var runtimeLbl: UILabel!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var voteAvgLbl: UILabel!
    @IBOutlet weak var genresLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchMovie()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetchMovie()
    }
}

// MARK: - Alamofire
extension DetailMovieVC {
    
    func fetchMovie() {
        let url = "https://api.themoviedb.org/3/movie/\(data?.id ?? 0)"
        let parameters: [String: String] = ["api_key": APIKEY,"language":"es"]
        AF.request(url,parameters: parameters)
            .validate()
            .responseDecodable(of: MovieDetail.self) { (response) in
                print(response)
                guard let movie = response.value else { return }
                self.movie = movie
                self.populateMovie()
        }
    }
    
    func populateMovie(){
        var genresStr = ""
        if let tmpList = self.movie?.genres { genresStr = (tmpList.map{$0.name}).map{String($0)}.joined(separator: ", ") }
        
        self.movieTitleLbl.text = movie?.title
        self.runtimeLbl.text = "\(movie?.runtime ?? 0) min"
        self.releaseDateLbl.text = movie?.releaseDate
        self.voteAvgLbl.text = "\(movie?.voteAverage ?? 0)"
        self.genresLbl.text = genresStr
        self.descLbl.text = movie?.overview
      //  self.movieImg.kf.setImage(with: URL(string: "https://image.tmdb.org/t/p/original" + movie!.backdropPath))
        if movie?.backdropPath != nil && movie!.backdropPath != "" {
            let url = URL(string: "https://image.tmdb.org/t/p/original" + movie!.backdropPath!)
            movieImg.kf.setImage(with: url)
        } else {
            
            movieImg.image = UIImage(named: "noImage")
        }
    }
}
