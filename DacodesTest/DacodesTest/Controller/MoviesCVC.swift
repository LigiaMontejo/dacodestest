//
//  MoviesCVC.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/26/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

import UIKit
import Alamofire

private let reuseIdentifier = "MovieCell"

class MoviesCVC: UICollectionViewController {
    
    
    var movies: [Movie] = []
    var selectedItem: Movie?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchMovies()
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationVC = segue.destination as? DetailMovieVC else {
            return
        }
        destinationVC.data = selectedItem
    }
    
}

// MARK: - UICollectionViewDataSource
extension MoviesCVC {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MovieCell
        
        // Configure the cell
        let movie = movies[indexPath.row]
        cell.movie = movie
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedItem = movies[indexPath.row]
        self.performSegue(withIdentifier: "SegueDetail", sender: nil)
    }
    
   
}

// MARK: - Alamofire
extension MoviesCVC {
  func fetchMovies() {
    let url = "https://api.themoviedb.org/3/movie/now_playing"
    let parameters: [String: String] = ["api_key": APIKEY,"language":"es"]
    AF.request(url,parameters: parameters)
        .validate()
        .responseDecodable(of: Movies.self) { (response) in
            print(response)
      guard let movies = response.value else { return }
      self.movies = movies.all
      self.collectionView.reloadData()
    }
  }
}

// MARK: - Collection View Flow Layout Delegate
extension MoviesCVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
           let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
           let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
           return CGSize(width: size, height: size)
       }

}
