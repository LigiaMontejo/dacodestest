//
//  Genre.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/27/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

struct Genre: Decodable {
    let id: Int
    let name: String

  enum CodingKeys: String, CodingKey {
    case id
    case name
  }
}
