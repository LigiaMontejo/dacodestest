//
//  Movie.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/26/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

struct Movie: Decodable {
    let id: Int
    let title: String
    let posterPath: String?
    let releaseDate:String
    let voteAverage:Double
 
  enum CodingKeys: String, CodingKey {
    case id
    case title
    case posterPath = "poster_path"
    case releaseDate = "release_date"
    case voteAverage = "vote_average"
  }
}
