//
//  MovieDetail.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/27/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

struct MovieDetail: Decodable {
    let id: Int
    let title: String
   // let posterPath: String?
    let releaseDate:String
    let voteAverage:Double
    let overview:String
    let runtime:Int
    let backdropPath:String?
    let genres:[Genre]
    
 
  enum CodingKeys: String, CodingKey {
    case id
    case title
   // case posterPath = "poster_path"
    case releaseDate = "release_date"
    case voteAverage = "vote_average"
    case overview
    case runtime
    case backdropPath = "backdrop_path"
    case genres
  }
}

