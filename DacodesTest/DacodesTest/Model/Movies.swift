//
//  Movies.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/27/20.
//  Copyright © 2020 APIUM. All rights reserved.
//


struct Movies: Decodable {
  let total_results: Int
  let all: [Movie]
  
  enum CodingKeys: String, CodingKey {
    case total_results
    case all = "results"
  }
}

