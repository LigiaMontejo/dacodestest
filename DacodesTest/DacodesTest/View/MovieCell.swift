//
//  MovieCell.swift
//  DacodesTest
//
//  Created by Ligia Montejo on 5/27/20.
//  Copyright © 2020 APIUM. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCell: UICollectionViewCell {
    var movie: Movie! {
        didSet {
            self.updateUI()
        }
    }
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }()
    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitleLbl: UILabel!
    @IBOutlet weak var releaseDateLbl: UILabel!
    @IBOutlet weak var voteAvgLbl: UILabel!
    @IBOutlet weak var bottomVw: UIView!
    
    func updateUI(){
        
        bottomVw.clipsToBounds = true
        bottomVw.layer.cornerRadius = 20
        bottomVw.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        
        movieTitleLbl.text = movie.title
        releaseDateLbl.text = movie.releaseDate
        voteAvgLbl.text = "\(movie.voteAverage)"
        
        /*
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy"

        let date: NSDate? = dateFormatterGet.date(movie.releaseDate)
        releaseDateLbl.text = dateFormatterPrint.string(from: date! as Date)*/
        
        if movie.posterPath != nil && movie.posterPath != "" {
            let url = URL(string: "https://image.tmdb.org/t/p/w500" + movie.posterPath!)
            movieImg.kf.setImage(with: url)
        } else {
            
            movieImg.image = UIImage(named: "noImage")
        }
    }
}
