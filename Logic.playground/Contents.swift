
func grid(rows:Int, columns: Int){
    if (rows == columns) && (rows > 1){
        if (rows % 2 == 0 ){ print("L")}
        else {print("R")}
    }
    else if rows > columns && columns > 1 {
        if columns % 2 == 0 { print("U")}
        else {print("D")}
    }
    else if columns > rows {
        if rows % 2 == 0 { print("L")}
        else {print("R")}
    }
    else if columns == 1 {
        if rows == 1 { print ("R") }
        else { print ("D")}
    }
}

grid(rows: 1,columns: 1)
grid(rows: 2,columns: 2)
grid(rows: 3,columns: 1)
grid(rows: 3,columns: 3)
