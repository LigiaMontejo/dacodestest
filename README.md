**Prueba iOS para reclutamiento DaCodes**

El presente repositorio contiene una aplicación de prueba que consiste en mostrar un listado de peliculas reciente y su detalle. 

---
## Requisitos para construir el proyecto

Los siguientes son los requisitos necesarios para construir y ejecutar el proyecto:

1. Versión min XCode 11

## Pasos para construir el proyecto

1. Descarga o clona el proyecto desde bitbucket 
2. Desde la terminal ubica la carpeta del proyecto
3. Ejecuta **Pod install** para actualizar los pods utilizados en el proyecto.
4. Una vez instalados los pods, abre el proyecto DacodesTest.xcworkspace
5. Desde el menu, selecciona: Product -> Build

---

## Pasos para ejecutar el proyecto

1. Selecciona el dispositivo deseado para ejecutar el proyecto
2. Click en Play para ejecutar el proyecto.
